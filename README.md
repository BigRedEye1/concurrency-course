# Concurrency Course

_"You don't have to be an engineer to be a racing driver, but you do have to have Mechanical Sympathy"_ – Jackie Stewart, racing driver

[Awesome Concurrency](https://gitlab.com/Lipovsky/awesome-concurrency)

## Инструкции

1) [Начало работы](docs/setup.md)
2) [Как сдавать задачи](docs/ci.md)

## Навигация

- [Задачи](/tasks)
- [Дедлайны](/deadlines)
- [Manytask](http://84.252.128.234:5222/)
